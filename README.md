# MENSU #

Mensu is a web service based on the Django framework, to
allow young scientists to publish worldwide experimental
setups and experiments which they do with an affordable
measurement box name **ExpEYES-17** (see http://expeyes.in);

Users of the service must create an account either as a
**scientist** or as a **visitor**.

## Scientist role ##

A user with a **scientist** account can create a set of experiments, 
an select on of them as the "active experiment". That implies to
connect an ExpEYES-17 box to a USB socket of the computer, define
a program which will control the measurement box, and define how
so-called **visitors** will be able to modify a few parameters of
the experiment.

The **scientist**'s responsability is to make an interesting
experiment or demonstration, with some publication to explain
the physics which rule that setup, and to allow other people to
control his/her experiment.

## Visitor role ##

A user with a **visitor** account can book a few sessions in advance
to make experiments based on the setup created by a **scientist**.
When the timeslot for a working session is booked, the **visitor**
must check the time difference between his/her local clock and the
remote laboratory's clock, and when the timeslot begins, the
**visitor** can load a set of parameters, as allowed by the **scientist**,
to prepare an original experiment, and then click a "GO" button to
run the experiment.

For every experiment run by the **visitor**, a measurements file will
be made available, bundled with a reminder of the set of parameters which
were used for this puprpose, and a video clip of the experiment in
"real time".

**Visitors** can make comments which will be attached to the experiment they
visited.

# Installation of the software #

As a **scientist** publishing a work is supposed to let it available
worlwide for a significant amount of time, power consumption matters.

It is a good idea to use low-power, yet powerful computers like
Raspberry PI to drive the process (see http://www.raspberrypi.org/).

Dependencies are:

  * a Debian system (Buster distribution at least)
  * Debian packages: 
    * python3-django,
	* python3-django-filters,
	* eyes17,
	* libjs-jquery,
	* libjs-jquery-ui,
	* libjs-codemirror (for syntax highliting),
	* jsxgraph         (to view graphic results),
	* ffmpeg,
	* pandoc,
  
... [work in progress] ...
