from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^manage', manage, name='manage'),
    url(r'^viewSummary', viewSummary, name='viewSummary'),
    url(r'^dlSummary', dlSummary, name='dlSummary'),
    url(r'^activateExperiment', activateExperiment, name='activateExperiment'),
    url(r'^deleteExperiment', deleteExperiment, name='deleteExperiment'),
    url(r'^dupliExperiment', dupliExperiment, name='dupliExperiment'),
    url(r'^setParameter', setParameter, name='setParameter'),
    url(r'^delParameter', delParameter, name='delParameter'),
    url(r'^delRecord', delRecord, name='delRecord'),
    url(r'^remoteLab', remoteLab, name='remoteLab'),
    url(r'^recordParams', recordParams, name='recordParams'),
    url(r'^readParams', readParams, name='readParams'),
    url(r'^imageWebcam', imageWebcam, name='imageWebcam'),
    url(r'^programWithParams', programWithParams, name='programWithParams'),
    url(r'^deleteExpRecord', deleteExpRecord, name='deleteExpRecord'),
    url(r'^viewExpRecord', viewExpRecord, name='viewExpRecord'),
    url(r'^listExpRecords', listExpRecords, name='listExpRecords'),
]
