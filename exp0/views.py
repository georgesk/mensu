from django.utils.translation import gettext as _
from django.utils.timezone import make_aware
from django.shortcuts import render, HttpResponseRedirect
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.utils.encoding import smart_str
from django.db.models import Q
from django.template.loader import render_to_string, get_template

import subprocess, os, json, re, time, datetime

from .models import Experiment, Parameter, ParamSetting, ExpRecord
from .forms import LoginForm, ExperimentForm
from .common import SUMMARY_DIR
import mensu.version
import mensu.settings

# Create your views here.

def inGroups(request, groups):
    """
    finds whether the user belongs to a set of groups
    @param request a Django request
    @param groups an iterable of group names
    @return True if the groups of the users intersect the given groups
    """
    userGroups=set([g.name for g in request.user.groups.all()])
    return userGroups & set(groups)

def editOneExperiment(request, form, id):
    """
    Edit one experiment given by its primary key, and
    one modelform
    @param request a Django request
    @param form a form (instance of ExperimentForm)
    @param id primary key for the experiment
    """
    exp=Experiment.objects.get(pk=int(id))
    params=Parameter.objects.filter(exp=exp).order_by("pk")
    return  render(request, 'manage.html',
                   {
                       "experiments" : [], # no experiment to show
                       "ef" : form,
                       "expId": id,
                       "params": params,
                   }
    )
    
def manage(request):
    """
    Web page to manage an experiment.
    Check that the user is logged in and member of the group "scientist"
    then give her a valuable tool to shape experiments.
    """
    if not inGroups(request, ['scientist']):
        return HttpResponseRedirect("/accounts/login/")
    expId=""
    if request.method == 'POST':
        ## do we want to edit a specific experiment?
        expId2edit=request.POST.get("expId2edit","")
        if expId2edit:
            # insert the data from the given experiment to the
            # New Experiment division, hide its heading, show
            # the form
            expId=int(expId2edit)
            exp=Experiment.objects.get(pk=expId)
            # do not edit parameters at this level
            experimentForm=ExperimentForm(instance=exp)
            return editOneExperiment(request, experimentForm, expId)
        experimentForm=ExperimentForm(request.POST)
        expId=request.POST.get("expId","")
        if experimentForm.is_valid():
            if expId:
                # we were editing an existing experiment
                # so, we must replace this experiment
                # rather than save a new one
                exp=Experiment.objects.get(pk=int(expId))
                # get new values from experimentForm
                new=experimentForm.save(commit=False)
                for field in ("title", "summary", "channels", "sessionDuration", "duration", "nInterval", "python"):
                    setattr(exp, field, getattr(new, field))
            else: 
                exp=experimentForm.save(commit=False)
                exp.box=1
                exp.owner=request.user
            exp.save()
            # do no return : so the page will be shown as when no POST was made
        else:
            # the form is not valid so reapply for it
            return editOneExperiment(request, experimentForm, expId)
            
    experimentForm=ExperimentForm(initial={"sessionDuration": 300, "duration": 2, "nInterval": 800, "channels" : "A1, A2"})
    experiments=Experiment.objects.filter(owner=request.user).order_by('pk')
    for exp in experiments:
        ## enrichment of experiments by their parameters
        exp.params=Parameter.objects.filter(exp=exp).order_by('pk')
    return render(
        request, 'manage.html',
        {
            "experiments" : experiments,
            "ef" : experimentForm,
            "expId": "", # the New Experiment should has no number
        }
    )

    
    
def activateExperiment(request):
    """
    Try to activate an experiment. The POST data should provide the key expId
    """

    ## find the active experiment
    activeExp=Experiment.objects.filter(active=True)
    id=None
    if(activeExp):
        id=activeExp[0].pk
    ## inactivate all
    for a in activeExp:
        a.active=False
        a.save()
    ## then activate the only one which is given by expId
    ## if expId is not the same as the formerly active experiment
    expId=int(request.POST.get("expId"))
    if expId==id:
        return JsonResponse({"msg": "ok"})
    try:
        exp=Experiment.objects.get(pk=expId)
        exp.active=True
        exp.save()
    except:
        pass
    return JsonResponse({"msg": "ok"})

def deleteExperiment(request):
    """
    Try to delete an experiment. The POST data should provide the key expId
    """
    expId=int(request.POST.get("expId"))
    try:
        exp=Experiment.objects.get(pk=expId)
        exp.delete()
    except:
        pass
    return JsonResponse({"msg": "ok"})

def dupliExperiment(request):
    """
    Try to duplicate an experiment. The POST data should provide the key expId
    """
    expId=int(request.POST.get("expId"))
    try:
        exp=Experiment.objects.get(pk=expId)
        exp.pk=None # to make a duplicate when the experiment is saved later
        exp.python=_("# copy of the experiment \"{e}\"\n").format(e=exp.title) + exp.python
        exp.title=_("Copy of ")+ exp.title
        exp.summary=_("copy_")+ exp.summary
        # never activate a duplicate
        exp.active=False
        exp.save()
        ## duplicate also the old experiment's parameters
        old=Experiment.objects.get(pk=expId)
        params=Parameter.objects.filter(exp=old).order_by("pk")
        for p in params:
            p.pk=None
            p.exp=exp
            p.save()
        ## the copy of an experiment needs no timeslots at the begin
    except:
        pass
    return JsonResponse({"msg": "ok"})

def olderFile(target, source):
    """
    Check whether a generated file (target) exists, and is older than its
    source file.
    """
    return os.path.isfile(target) and os.path.getmtime(target) > os.path.getmtime(source)

def viewSummary(request):
    """
    Given a RST file as the GET argument "rst", makes the HTML file
    from it and returns its content
    """
    rstFile=os.path.join(SUMMARY_DIR, request.GET.get("rst"))
    htmlFile=rstFile[:-4]+".html"
    if not olderFile(htmlFile, rstFile):
        command = "cd {sd}; pandoc -s -o {target} {source}; ./imgInliner.py {target}".format(sd=SUMMARY_DIR, target=htmlFile, source=rstFile)
        subprocess.call(command, shell=True)
        # images must be inlined
    assert(os.path.isfile(htmlFile))
    return HttpResponse(open(htmlFile).read())

def dlSummary(request):
    """
    Given a RST file as the GET argument "rst", let download it.
    """
    rstFile=os.path.join(SUMMARY_DIR, request.GET.get("rst"))
    htmlFile=rstFile[:-4]+".html"
    if not olderFile(htmlFile, rstFile):
        command = "cd {sd}; pandoc -s -o {target} {source}; ./imgInliner.py {target}".format(sd=SUMMARY_DIR, target=htmlFile, source=rstFile)
        subprocess.call(command, shell=True)
        # images must be inlined
    assert(os.path.isfile(htmlFile))
    response = HttpResponse(
        open(rstFile).read(),
        content_type='text/x-rst',
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(request.GET.get("rst"))
    return response

def setParameter(request):
    id=request.POST.get("id")
    exp=request.POST.get("exp")
    symbol=request.POST.get("symbol")
    minval=request.POST.get("minval")
    maxval=request.POST.get("maxval")
    option=request.POST.get("option")
    comment=request.POST.get("comment")
    if (id):
        p=Parameter.objects.get(pk=int(id))
        p.symbol=symbol
        p.minval=minval
        p.maxval=maxval
        p.option=option
        p.comment=comment
        p.save()
    else:
        p=Parameter(
            exp=Experiment.objects.get(pk=int(exp)),
            symbol=symbol,
            minval=minval,
            maxval=maxval,
            option=option,
            comment=comment,
        )
        p.save()
    return JsonResponse({"message": "ok"})

def delParameter(request):
    id=request.POST.get("id")
    p=Parameter.objects.get(pk=int(id))
    p.delete()
    return JsonResponse({"message": "ok"})

def recordParams(request):
    """
    Records parameters tuned at page /exp0/remoteLab into the database
    """
    name=request.POST.get("name",None)
    expId=int(request.POST.get("exp",0))
    userId=int(request.POST.get("user",0))
    values=request.POST.get("values",None)
    msg="ok"
    errors=[]
    try:
        ps=ParamSetting(
            owner=User.objects.get(pk=userId),
            exp=Experiment.objects.get(pk=expId),
            name=name,
            values=values,
        )
        ps.save()
    except Error as err:
        errors.append(err)
    return JsonResponse({
        "msg": msg,
        "errors": errors,
        "recordId": ps.pk,
        "recordStr": str(ps)
    })

def readParams(request):
    """
    read the database to get parameter values stored in the database
    """
    ps=ParamSetting.objects.get(pk=int(request.POST.get("id")))
    return JsonResponse({
        "msg": "ok",
        "id": ps.pk,
        "title": str(ps),
        "values": json.loads(ps.values)
    })

def delRecord(request):
    """
    deletes parameter values stored in the database
    """
    id=int(request.POST.get("recId"))
    ParamSetting.objects.get(pk=id).delete()
    return JsonResponse({
        "msg": "ok deleted record",
        "id": id,
    })

def remoteLab(request):
    """
    Implement the web page of the remote laboratory
    """
    from subprocess import call, Popen, PIPE
    ### somebody requested the page /exp0/remoteLab,
    ## so the webcam is supposed to be joinable
    out,_=Popen("ps ax | grep 'python3.*joinableWebcam'| grep -v grep",
                shell=True,
                stdout=PIPE).communicate()
    if len(out)==0:
        ## when no webcam is joinable, try once to fire it.
        cmd="(cd {path}; ./joinableWebcam.py pipeTasks &)".format(
            path=mensu.settings.ROOT_PATH)
        call(cmd, shell=True)
    else:
        print("ERROR? joinableWebcam was already there")
    scientists=User.objects.filter(groups__name="scientist")
    exps = Experiment.objects.filter(active=True)
    if not exps:
        return render(request,"remoteLab.html",{
            "exp": None,
            "scientists": scientists,
        })
    params=Parameter.objects.filter(exp=exps[0])
    records=ParamSetting.objects.filter(owner=request.user, exp=exps[0]).order_by('whenCreated')
    expRecords=ExpRecord.objects.filter(paramSet__owner=request.user).order_by('whenCreated')
    return render(request,"remoteLab.html",{
        "exp": exps[0],
        "params": params,
        "records" : records,
        "expRecords": expRecords,
    })

def imageWebcam(request):
    """
    Read the image at settings/webcam.jpg, then ask for a refresh to
    the joinable webcam, and return the previous image's content
    """
    imagePath=os.path.join(mensu.settings.MEDIA_ROOT, "webcam", "webcam.jpg")
    image=open(imagePath,"rb").read()
    ## query  the joinable webcam for the next image
    print(imagePath, file=open(mensu.settings.PIPE_TASKS,"w"))
    ## return the last image's content
    response=HttpResponse(image, content_type='image/jpeg')
    response['Cache-control']="max-age=0, must-revalidate"
    return response

def programWithParams(request):
    """
    inserts the set of current parameters into the program and
    returns the source code as a json response, when there is no
    parameter "run"; else the program is launched, a request for a video
    file is submitted to the "joinable webcam", the experiment's results are
    recorded into the database, and a json response is returned, which
    contains the results (stdout and stderr), and the name of the
    expected video file.
    """
    id=int(request.POST.get("id"))
    pset=ParamSetting.objects.get(pk=id)
    program=pset.exp.python
    ps=json.loads(pset.values)
    for paramName in ps:
        value=ps[paramName]
        m=re.match(r"^([-\S]+)\s.*\s([-\S]+)$", value)
        if m:
            program=program.replace(paramName+"_l", m.group(1))
            program=program.replace(paramName+"_h", m.group(2))
        else:
            program=program.replace(paramName, value)
    if "run" in request.POST:
        t=time.time()
        dt=make_aware(datetime.datetime.fromtimestamp(t))
        videofile="video-%s.avi" % int(t)
        mensu.settings.videoQuery(videofile, pset.exp.duration)
        p=subprocess.Popen("python3", shell=True,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
        )
        p.stdin.write(program.encode("utf-8"))
        stdout, stderr = p.communicate()
        er=ExpRecord(
            paramSet=pset, stdout=stdout,stderr=stderr,
            videofile=videofile, whenCreated=dt
        )
        er.save()
        return JsonResponse({
            "ok": True,
            "id": er.pk,
            "time": er.whenCreated,
            "stdout": stdout.decode("utf-8"),
            "stderr": stderr.decode("utf-8"),
            "videourl": er.videoUrl,
        })        
    else:
        return JsonResponse({
            "ok": True,
            "source": program,
        })

def deleteExpRecord(request):
    """
    delete an ExpRecord
    """
    id= int(request.POST.get("id"))
    ExpRecord.objects.get(pk=id).delete()
    return JsonResponse({
        "ok": True,
    })

def listExpRecords(request):
    """
    gives a list of experiments' records, which have the same owner,
    as a json response, with a list of HTML code for table rows
    """
    id=int(request.POST.get("owner"))
    expRecords=ExpRecord.objects.filter(paramSet__owner__pk=id).order_by("whenCreated")
    rows=[render_to_string("expRecordRow.html", {
        "er": er,
    },) for er in expRecords]
    return JsonResponse({
        "ok": True,
        "rows": rows,
    })
    

def viewExpRecord(request):
    """
    create a web page to view experiment results
    this page is accessed via a GET request, so the key is not a little
    integer, but a complete date/time, which must be accurate.
    """
    date=request.GET.get("date")
    er=ExpRecord.objects.get(whenCreated=date)
    return render(request,"expRecord.html",{
        "er": er,
    })
    
