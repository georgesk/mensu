from django.utils.translation import gettext as _
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

import os

from .models import Experiment
from .common import SUMMARY_DIR


class LoginForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'password',)
        widgets = { 'password': forms.PasswordInput, }

class ExperimentForm(forms.ModelForm):

    class Meta:
        model=Experiment
        fields = (
            # read-only 'box',
            # read-only 'owner',
            'channels',
            'sessionDuration',
            'duration',
            'nInterval',
            'title',
            'summary',
            'python',
        )
    def clean_channels(self):
        """
        Verify that channels for the measurements are really allowed
        channels
        """
        allowed_channels=set("A1,A2,A3,MIC,IN1".split(","))
        # given channels can be with tabs or spaces, but must be
        # separated by commas.
        given_channels=set([c.strip() for c in self.cleaned_data["channels"].split(",")])
        remaining = given_channels - allowed_channels
        if remaining:
            # there is at least on non-allowed channel remaining
            raise ValidationError(_("Not recognized: `{r}`. Allowed channels are: {l}").format(r=remaining, l=allowed_channels))
        return self.cleaned_data["channels"]

    def clean_sessionDuration(self):
        """
        A session duration must be comprized between 5 and 30 minutes
        """
        mmin=5
        mmax=30
        sd=self.cleaned_data["sessionDuration"]
        if not (mmin*60 <= sd <= mmax*60):
            raise ValidationError(_("This value must be in the interval {smin} to {smax} ({mmin} to {mmax} minutes).").format(smin=mmin*60, smax=mmax*60, mmin=mmin, mmax=mmax))
        return sd

    def clean_duration(self):
        """
        The longest experinment duration is 30 seconds, the shortest is 
        1 millisecond
        """
        smin=1e-3
        smax=30
        s=self.cleaned_data["duration"]
        if not smin<=s<=smax:
            raise ValidationError(_("This value must be in the interval {smin} to {smax}.").format(smin=smin, smax=smax))
        return s
    
    def clean_nInterval(self):
        """
        The intervals are between 1 and 2000
        """
        imin=1
        imax=2000
        i=self.cleaned_data["nInterval"]
        if not imin<=i<=imax:
            raise ValidationError(_("This value must be in the interval {imin} to {imax}.").format(imin=imin, imax=imax))
        return i

    def clean_python(self):
        """
        Compilation faults are reported.
        """
        python=self.cleaned_data["python"]
        errs=""
        try:
            ast=compile(python,'<string>', 'exec')
        except SyntaxError as e:
            errs+=str(e)
        except ValueError as e:
            errs+=str(e)
        if errs:
            raise ValidationError(errs)
        return python

    def clean_summary(self):
        """
        Checks that the summary is the name of a file in the
        subdirectory summaries; if the file does not exist, the
        file .template_en.rst is copied to the given file name.
        """
        summary=self.cleaned_data["summary"]
        if not summary.endswith(".rst"):
            summary+=".rst"
        target=os.path.join(SUMMARY_DIR, summary)
        if not os.path.isfile(target):
            with open(os.path.join(SUMMARY_DIR,'.template_en.rst')) as infile:
                with open(target,"w") as outfile:
                    outfile.write(infile.read())
        assert (os.path.isfile(target))
        return summary
            
                          
