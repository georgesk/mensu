from django.utils.translation import gettext as _
from django.utils.text import mark_safe
from django.db import models
from django.contrib.auth.models import User

import os, json, re

from mensu.settings import BASE_DIR, VIDEO_URL, VIDEO_ROOT

# Create your models here.

class ImageUser(models.Model):
    user=models.ForeignKey(User)
    image=models.FileField(upload_to='images')
    
class Experiment(models.Model):

    """
    When there are more than 1 Eyes17 box, each one is identified
    by a number. The same holds for webcams (only when there will be
    some reliable way to give numbers to devices in a reliable way
    across reboots)
    """
    box = models.IntegerField(
        verbose_name=_("Box number"),
        default=1
    )

    """
    The owner of the experiment (she must be part of the group "scientist"
    """
    owner = models.ForeignKey(User)
    
    """
    Experiments have a one-to-many relationship with Parameters
    no field here so far, as class Parameter has a foreign key to Experiment
    """

    """
    Experiments have a one-to-many relationship with Timeslots
    no field here so far, as class Timeslot has a foreign key to Experiment
    """

    """
    Experiments have one result: a matrix of numbers, which is collected
    during a predefined time, repeteadly with some delay. The property
    channels bears the list of channels which are measured; the
    zeroth one is implicitely the time.
    """
    channels=models.CharField(max_length=50)

    """
    Duration for an experimental session (when a visitor will be able to
    play with the experiment).
    """
    sessionDuration=models.IntegerField()

    """
    Duration of a single measurement, which is made when the visitor
    clicks the "GO" button.
    """
    duration=models.FloatField()

    """
    Number of time intervals for the measurement (for N intervals, the
    number of measurements should be N+1). This way to specify the delay
    between successive measurements makes it easier for one to get simple
    date values in the returned matrix of numbers.
    """
    nInterval=models.IntegerField()

    """
    Title of the experiment. No longer than a tweet message!
    """
    title = models.CharField(max_length=140)

    """
    Summary of the experiment. It must be the name of a file
    which will be edited in Restructured Text format. The file name
    should remain short, and managed completely at instance creation time.
    Two summaries cannot share the same file.
    """
    summary=models.CharField(max_length=25)

    """
    Python code of the experiment. Modifiable parameters must be
    written as PARAMn, where n is a number (i.e. PARAM0, PARAM1, ...
    PARAM9). When a visitor clicks on the "GO" button, parameters
    are set from the form she filled.
    """
    python=models.TextField()

    """
    An experiment among the complete collection can be selected as
    active.
    """
    active=models.BooleanField(default=False)

    def __str__(self):
        return self.title

    @property
    def summaryHtmlUrl(self):
        """
        provide an Url to get the styled HTML of the summary
        """
        return "/exp0/viewSummary?rst="+self.summary

class Parameter(models.Model):
    """
    Defines an experiment parameter; each one is described by
    properties relative to Eyes17's features
    """

    """
    Link to the parent Experiment instance
    """
    exp = models.ForeignKey(Experiment)

    """
    Name of the symbol to be controlled
    """
    symbol = models.CharField(max_length=4)

    """
    Minimum allowed value
    """
    minval = models.FloatField()

    """
    Maximum allowed value
    """
    maxval = models.FloatField()

    """
    Optional property. Some outputs can have an optional property,
    like WG, which can be triangle, a sine wave or come from a given
    wave table
    """
    option = models.CharField(max_length=50, default="", blank=True)

    """
    Optional property. useful to comment the very short symbol name
    """
    comment = models.CharField(max_length=150, default="", blank=True)

    def __str__(self):
        # Translators: symbol is some short string, exp is for some experiment
        return _("{symbol} for {exp}").format(symbol=self.symbol, exp=self.exp)

    @property
    @mark_safe
    def widget(self):
        """
        provides the HTML code for a widget which will allow visitors
        to modify the parameter interactively
        """
        helpStr=(_("You can tune the values precisely by editing them in this text field."))
        dataSet="""
data-vmin="{minval}" data-vmax="{maxval}" data-sep="{sep}"\
""".format(minval=self.minval, maxval=self.maxval, sep=_("to"))
        if "range" in self.option:
            dataSet+=" data-range='true'"
        if "log" in self.option:
            dataSet+=" data-log='true'"
        html="""\
<input type="text" class="paramWidget" {dataSet} title="{helpStr}" id="{s}" />\
""".format(dataSet=dataSet, helpStr=helpStr, s=self.symbol)
        return html

class ParamSetting(models.Model):
    """
    to keep a record of parameters settings as a list (which can contain
    sublists) of numeric values.
    """
    owner = models.ForeignKey(User)
    exp   = models.ForeignKey(Experiment)
    name   = models.CharField(max_length=25)
    whenCreated = models.DateTimeField(auto_now=True)
    values = models.CharField(max_length=150)

    def __str__(self):
        return "{n} ({d})".format(n=self.name, d=self.whenCreated.strftime("%Y/%m/%d %H:%M"))

    @property
    @mark_safe
    def widgetReadOnly(self):
        """
        provides the HTML code for a widget which will allow visitors
        to view a parameter set
        """
        dico=json.loads(self.values)
        result=""
        for k in dico:
            param=Parameter.objects.get(exp=self.exp, symbol=k)
            result+="<div title='{c}'><span class='key'>{k}</span><span class='val'>{v}</span></div>".format(c=param.comment, k=k, v=dico[k])
        return result

            
    
class Timeslot(models.Model):
    """
    defines a timeslot when an experiment is booked by a visitor;
    during a timeslot, the visitor will be able to push the "GO" button
    of the experiment at will, and she will be given references to
    records of the experiments.
    """

    """
    Link to the parent Experiment instance
    """
    exp = models.ForeignKey(Experiment)

    """
    Reservation's date
    """
    whenCreated = models.DateTimeField(auto_now=True)

    """
    Begin of the experiment session. The end of the experiment session
    is given by the duration decided by authors of the experiment
    """
    begin =  models.DateTimeField()

    """
    Identification of the visitor. Visitors are prompted to create an account
    as a Django user
    """
    user = models.ForeignKey(User)
    
class ExpRecord(models.Model):
    """
    Collects the data coming from an experiment run with a particular
    set of parameters
    """

    paramSet = models.ForeignKey(ParamSetting)
    whenCreated = models.DateTimeField(auto_now=True)
    stdout = models.TextField()
    stderr = models.TextField()
    videofile = models.CharField(max_length=140)

    def __str__(self):
        return "Experiment recorded: %s" % self.whenCreated

    @property
    def videofileRecoded(self):
        """
        N.B.: due to openCV's limitations, the video file is first
        recorded as MJPG-encoded AVI file, then recoded to V8-encoded
        WEBM file, for a better compression and compatibility with
        HTML readers. Only the WEBM file is stored in the filesystem,
        the AVI file is unlinked as soon as it was recoded.
        """
        return re.sub(r"\.avi$", ".webm", self.videofile)

    @property
    def videoUrl(self):
        """
        gives the URL to download self.videofile
        @return a valid URL served by Mensu.
        """
        return VIDEO_URL+self.videofileRecoded
    
    def delete(self):
        """
        overrides the inherited method, by unlinking the videofile
        additionnally
        """
        models.Model.delete(self)
        videofile = os.path.join(VIDEO_ROOT,self.videofileRecoded)
        if os.path.exists(videofile):
            os.unlink(videofile)
        return
    
