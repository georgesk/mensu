/**
 * this function builds a JQuery UI Slider with exponential steps
 * to read the results, the simplest way is to read the value of the
 * input and parse it.
 *
 * @param inp an input element (selector, HTML node or jquery)
 *   the  data-value attribute in this element is used to guess features
 *   which are vmin, vmax (minimum and maximum values), the localized
 *   separator to display values, "range option" when a range is wanted
 *   instead a single value, and "log option" when a logarithmic slider
 *   is more relevant than a linear one.
 * @param options an object with properties to override previous guesses
 *   when it is defined: properties are vmin, vmax, sep (the separator),
 *   range (to make a range rather than a simple slider if true) and
 *   log   (to make the slider exponential rather than linear if true)
 * @return a div attached to an initialized jQuery slider object
 **/
function createSlider(inp, options) {
    // defaults
    var vmin=0;
    var vmax=5;
    var range=false;
    var log=false;
    var sep="to";
    var match;
    /**
     * parse the attribute data-value if any ...
     * try to guess variables min, max, the separator, and options
     * range, log from the initial value of edit. The regular expression
     * should match a number, then a separator, and another number.
     **/
    var regularExpr=/([-\S]+) +(.+) +([-\S]+)/;
    var edit=$(inp);
    if (edit.attr('data-value')){
	var match = edit.attr('data-value').match(regularExpr);
	if(match){
	    vmin = parseFloat(match[1]);
	    sep  = match[2];
	    vmax = parseFloat(match[3]);
	    if(isNaN(vmin)||isNaN(vmax)) {
		vmin=0;
		vmax=5;
	    }
	    /**
	     * if the separator contains more than one word
	     * let's assume that a range is wanted. Examples of such separators:
	     * "up to", "jusqu'à"
	     **/
	    range = sep.split(/[ ']/).length > 1;
	    /**
	     * if vmin is very near zero, compared to vmax, but not zero,
	     * let's assume that a log slider is wanted.
	     **/
	    if(vmin!=0 && vmax/vmin > 1000) log=true;
	}
    }
    /**
     * get the options. Option fields override previous guesses if any.
     **/
    if(options==undefined) options={}; // empty object by default
    if("vmin"  in options) vmin  = options.vmin;
    if("vmax"  in options) vmax  = options.vmax;
    if("sep"  in options) sep  = options.sep;
    if("range"  in options) range  = options.range;
    if("log"  in options) log  = options.log;
    // linear conversion functions by default
    var slider2edit = function(x){return x*(vmax-vmin)/1000+vmin;};
    var edit2slider = function(v){v=parseFloat(v); return (v-vmin)/(vmax-vmin)*1000;};
    /**
     * When the log option is active, conversions will be based on
     * exponential and logarithm, and the minimum cannot be zero.
     **/
    if(log){
	if(vmin==0) vmin=1e-6;
	var m = vmin;
	var p = 1000/Math.log10(vmax/vmin)
	slider2edit= function(x){return m*Math.pow(10,x/p);};
	edit2slider= function(v){v=parseFloat(v); return p*Math.log10(v/m);};
    }
    var sliDiv=$("<div>");
    sliDiv.insertBefore(edit)
    function showNumber(n){
	//two digit after dot; normal if n>=1 else eventually exponential
	if(!log || n>=1) return parseInt(Math.round(n*100))/100;
	else return n.toExponential(2);
    }
    function display(v, fun){
	// show number v in the input widget edit
	// passed through function fun
	edit.val(showNumber(fun(v)));
    }
    function display2(v0, v1, fun){
	// show numbers v0 and v1 in the input widget edit, separated by sep
	// and passed through function fun
	edit.val(showNumber(fun(v0))+"  "+sep+"  "+showNumber(fun(v1)));
    }
    var slider;
    if (range){
	slider = sliDiv.slider({
	    range: true, min: 0, max: 1000, step: 5, values: [0, 1000],
	    slide: function( event, ui ) {
		display2(ui.values[0], ui.values[1], slider2edit);
	    },
	});
    } else {
	slider = sliDiv.slider({
	    min: 0, max: 1000, step: 5, value: 500,
	    slide: function( event, ui ) {
		display(ui.value, slider2edit);
	    },
	});
    }
    if (range) display2(0, 1000, slider2edit);
    else display(500, slider2edit);
    edit[0].oninput = function(){
	edit.css("background", "white");
	if(range){
	    var m=edit.val().match(regularExpr);
	    if (m && !isNaN(parseFloat(m[1])) && !isNaN(parseFloat(m[3]))){
		sliDiv.slider("values", [edit2slider(m[1]), edit2slider(m[3])]);
	    } else {
		// complain, the syntax is incorrect
		edit.css("background", "pink");
	    }
	} else {
	    if (!isNaN(parseFloat(edit.val()))){
		sliDiv.slider("value", edit2slider(edit.val()));
	    } else {
		edit.css("background", "pink");
	    }
	}
    };
    return sliDiv;
}

/**
 * updates the select with recorded sets of values
 **/
function updateParamSelect(data){
    if (data.errors.length){
	alert(JSON.stringify(data.errors));
    } else {
	var option=$("<option>", {value: data.recordId}).text(data.recordStr);
	$("#records").append(option);
	$("#records").show();
    }
}

/**
 * Recording the values set of parameters, as a callback.
 * @param form the form in the pannel "Parameters" of remote lab
 **/
function recordParams(form){
    form=$(form);
    var d=$("#paramDialog");
    function recordTheParams(name){
	// does the job
	var values={}
	var edits=form.find(".paramWidget");
	for (var i=0; i < edits.length; i++){
	    var e=$(edits[i]);
	    values[e.attr("id")]=e.val();
	}
	var data={
	    name: name,
	    user: form.find("#user").val(),
	    csrfmiddlewaretoken: form.find("input[name='csrfmiddlewaretoken']").val(),
	    exp: form.find("#exp").val(),
	    values: JSON.stringify(values),
	}
	$.post('/exp0/recordParams', data
	      ).done(
		  updateParamSelect,
	      ).fail(
		  function(data){
		      // something went wrong with the transmission
		      alert(JSON.stringify(data));
		  },
	      );
    }
    d.dialog({
	title: "Record's short name",
	modal: true,
	buttons: [
	    {
		text: "Cancel",
		click : function(){
		    $(this).dialog( "close" );
		}
	    },
	    {
		text: "OK",
		click: function(){
		    recordTheParams($("#name").val());
		    $(this).dialog( "close" );
		}
	    },
	],
    });
    return false;
}

function delRec(recId){
    // send an ajax request to delete the record
    $.post("/exp0/delRecord", {
	csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
	recId: recId,
    }).done (
	// if it's OK, delete it from the select
	function(data){
	    var r=$("#records");
	    r.find("option[value='"+data.id+"']").remove();
	    // hide the select when it's empty
	    if (r.find("option").length==1){
		// the only remaining line is the "chooser" with no value
		r.hide();
	    }
	    // change the title to remove the "forget it" link
	    $("#paramSetTitle").text("Current setting");
	},
    );
}

/**
 * Callback function for the parameter setting's records select
 **/
function recordChanged(sel){
    sel=$(sel);
    var option=sel.find(":selected");
    var id=option.val()
    if (isNaN(id)) return;
    $.post("/exp0/readParams",{
	csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
	id: id,
    }).done(
	function(data){
	    /* set a title to show that we loaded a record */
	    var h2=$("#paramSetTitle")
	    h2.text($("#rsettings").text()+" " +data.title);
	    var a=$("<a>",{href: "javascript:delRec("+data.id+")"});
	    a.text($("#forget").text());
	    h2.append(a);
	    /* modify the edits */
	    for (var symbol in data.values){
		$("#"+symbol).val(data.values[symbol]);
		$("#"+symbol).trigger('input');
	    }
	}
    );
    sel.val(sel.find(".default").text());
}

var editorInControlPanel="";

/**
 * Callback function bounds to the select "records2"
 **/
function record2Changed(){
    var r2=$("#records2");
    var option=r2.find(":selected");
    var id=option.val()
    $.post("/exp0/readParams",{
	csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
	id: id,
    }).done(
	function(data){
	    /* set a title to show that we loaded a record */
	    /* modify the spans */
	    for (var symbol in data.values){
		$("#ro-"+symbol).text(data.values[symbol]);
	    }
	}
    );
    // populate the program textarea with the source
    $.post("/exp0/programWithParams",{
	csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
	id: id,
    }).done(
	function(data){
	    var pwp = $("#pythonWithParams")
	    // first close any previous codemirror editor
	    if(editorInControlPanel!=""){
		editorInControlPanel.toTextArea();
	    }
	    // then feed the source into the textarea
	    $("#pythonWithParams").val(data.source);
	    editorInControlPanel=CodeMirror.fromTextArea(pwp[0], {
		lineNumbers: true
	    });
	    // mark the python code as readonly
	    editorInControlPanel.doc.markText({line: 0, ch:0}, {line: 10000, ch:0},{readOnly: true});
	}
    );
}

/**
 * callback function bound to the change event of the tab "Control"
 **/
function copyParams(){
    // this function is called only when the panel "Control" is checked *up*
    // it updates completely "records2" as some options could be created
    // during an interaction in the Params panel
    var r2=$("#records2");
    r2.find("option").remove();
    var empty=true;
    var first=true;
    $("#records option").each(function(i, el){
	el=$(el);
	if (parseFloat(el.val())){
	    var option=el.clone();
	    if(first){
		option.attr("selected","selected");
		first=false;
	    }
	    r2.append(option);
	    empty=false;
	}
    });
    if (empty) {
	r2.hide();
	$("#emptyRecord2").show()
    } else {
	r2.show();
	$("#emptyRecord2").hide()
	// populate the parameter set table with the first record
	record2Changed();
    }
    
}


/********* Refresh the webcam when the Control panel is visible **********/
function refreshWebcam(){
    var tc=$("#tabbedControl");
    if (tc.length==0) return
    if(tc[0].checked){
	var url="/exp0/imageWebcam-"+new Date().getTime();
	$("#webcam img").attr("src", url);
    }
    // launch the same function every 2 seconds
    window.setTimeout(refreshWebcam, 2000);
}

/**
 * Callback function for the "GO" button
 **/
function goFunction(){
    // first get the current set of parameters' id
    var id=$("#records2 option:selected").val();
    // then call the Python program with parameters applied
    var wait=$("#wait");
    wait.show();
    $.post("/exp0/programWithParams", {
	csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
	id: id,
	run: "run",
    }).done(function(data){
	// The object data returned as a Json response
	// gives the primary key to access a database record, and some
	// of its data, the stdout and stderr strings yielded by the
	// program, and a file name to retrieve a video recorded during the
	// program's run
	$("#stdout").val(data.stdout);
	$("#stderr").val(data.stderr);
	$("#videofile").attr("href", data.videourl).show();
	wait.hide();
    }).fail(function(data){
	wait.hide();
	alert("something is wrong with the call to goFunction");
    });
}

/**
 * callback function to delte an experiment record
 * @param erId the primary key of this record
 **/
function deleteExpRecord(erId){
    $.post("/exp0/deleteExpRecord", {
	csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
	id: erId,
    }).done(function(data){
	$("#row_experiment_"+erId).remove();
    });
}

/**
 * Copy the text in textarea #stdout into the clipboard
 * @return True if the copy is successful
 **/
function stdoutCp(){
    var o=$("#stdout");
    o.removeAttr("disabled");
    o.focus();
    o.select();
    var result=document.execCommand('copy');
    o.attr("disabled", "disabled");
    return result;
}

/**
 * Use the data in #stdout to plot a chart into
 * #plot canvas element
 **/
function stdoutPlot(){
    var color=["black", "red", "blue", "green", "purple", "brown"];
    var data=$("#stdout").val().split('\n');
    var x=[], yy=[];
    // the first line is supposed to contain at least one X value,
    // followed by one or more Y values
    var begin=0, d, l, searching=true;
    while (searching  && begin < data.length){
	// search the first line with a series of numbers
	d=data[begin].split(/\s+/);
	var allNumeric=true;
	for (var j=0; j < d.length; j++){
	    if (isNaN(parseFloat(d[j]))) allNumeric=false;
	}
	if(allNumeric) break;
	begin++;
    }
    var d=data[begin].split(/\s+/);
    var l=d.length;
    var xmin=xmax=parseFloat(d[0]);
    var ymin=ymax=parseFloat(d[1]);
    // add a sub-array to yy for every additional column, past the first one
    for (var i=1; i<l; i++) yy.push([]);
    // get all the numbers now
    for (var i =begin; i<data.length; i++){
	d = data[i].split(/\s+/);
	var v;
	if (l==d.length){ // beware the empty lines
	    v=parseFloat(d[0]);
	    x[i]=v;
	    if(v>xmax) xmax=v;
	    if(v<xmin) xmin=v; 
	    for (var j=1; j<l; j++){
		v=parseFloat(d[j])
		yy[j-1][i]=v;
		if(v>ymax) ymax=v;
		if(v<ymin) ymin=v; 
	    }
	}
    }
    $("#plot").show();
    var brd = JXG.JSXGraph.initBoard('plot', {boundingbox: [-10, 10, 10, -10], axis:true});
    var m=0.08; // the margin as a part of (max-min)
    brd.setBoundingBox([(1+m)*xmin-m*xmax, (1+m)*ymax-m*ymin,
			(1+m)*xmax-m*xmin, (1+m)*ymin-m*ymax]);
    console.log("GRRRR", xmin, ymax, xmax, ymin);
    console.log("GRRRR", (1+m)*xmin-m*xmax, (1+m)*ymax-m*ymin,
			(1+m)*xmax-m*xmin, (1+m)*ymin-m*ymax);
    for (var j=1; j<l; j++){
	brd.create('curve',[x,yy[j-1]],
		   {strokeColor:color[(j-1)%color.length]});
    }
    brd.update();
}

/**
 * callback function to refresh the experiments' records, as some
 * might be created without reloading the page
 **/
function refreshExpRecords(){
    var t=$("#expRecords");
    // keep the first two lines, remove the remainder
    t.find("tr[id^='row_experiment_']").remove();
    $.post('/exp0/listExpRecords',{
	csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
	owner: $("#userpk").val(),
    }).done(function(data){
	for(var i=0; i<data.rows.length; i++){
	    var r=$(data.rows[i]);
	    t.append(r);
	}
    });
}

$(function(){
    /**
     * the class expandableV should be used for elements which need to
     * use as much horizontal and vertical space as possible
     **/
    var expandableV=$(".expandableV");
    var verticalMargins=225; // experimentally; the iframe required extra space?
    expandableV.css({
	width: "100%",
	height: $(window).height()-verticalMargins,
    });
    $(window).on('resize', function(){
	expandableV.css({
	    height: $(this).height()-verticalMargins,
	});

    });
    /**
     * Initialize parameters' widgets
     **/
    var paramWidgets=$(".paramWidget");
    for(var i=0; i < paramWidgets.length; i++){
	var p=$(paramWidgets[i]);
	var options = {};
	if (p.attr("data-range")) options.range=true;
	if (p.attr("data-log"))   options.log=true;
	if (p.attr("data-vmin"))  options.vmin=parseFloat(p.attr("data-vmin"));
	if (p.attr("data-vmax"))  options.vmax=parseFloat(p.attr("data-vmax"));
	if (p.attr("data-sep"))   options.sep=p.attr("data-sep");
	createSlider(p, options)
    }
    /* bind the change signal of tabbedControl, and launch it once */
    $("#tabbedControl").change(copyParams);
    copyParams();
    /* bind the change signal of historyPanel, and launch it once */
    $("#historyPanel").change(refreshExpRecords);
    refreshExpRecords();
    /* call once the function refreshWebcam which will recal itsself later */
    refreshWebcam()
});
