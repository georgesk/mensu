/**
 * Javascript files used only for the url exp0/manage
 *
 **/

var csrf = "";
var editors = [];

$(function(){
    // launched at load time
    csrf=$("input[name='csrfmiddlewaretoken']").val();

    /**
     * make a check of the python string at this level since
     * Django is fooled by CodeMirror
     */
    function checkPython(){
	var python=$("textarea[name='python']").val();
	if (python.length==0){
	    alert("Please fill out at least one comment for the Python program");
	    return false;
	}
	return true;
    }
    /* not used: Django considers the click on the submit button.
    $("#expForm").submit(checkPython);
    */
    $("#submitButton").click(checkPython);
    
    $(".actions .edit").click(
	function(){
	    editExperiment($(this).attr("data-exp-id"));
	}
    );
    $(".actions .active").click(
	function(){
	    activateExperiment($(this).attr("data-exp-id"));
	}
    );
    $(".actions .dupli").click(
	function(){
	    dupliExperiment($(this).attr("data-exp-id"));
	}
    );
    $(".actions .delete").click(
	function(){
	    deleteExperiment($(this).attr("data-exp-id"));
	}
    );
    // activate Codemirror
    var textareas=$("textarea.python");
    for (var i=0; i<textareas.length; i++){
	var editor=CodeMirror.fromTextArea(textareas[i],
	    {
		lineNumbers: true,
		mode: "python",
		indentUnit: 2,
	    });
	function updateTextArea() {
            editor.save();
	}
	editor.on('change', updateTextArea);
	// make the python code readonly if the textarea element
	// has the class readonly
	if ($(textareas[i]).hasClass("readonly")){
	    editor.doc.markText({line: 0, ch:0}, {line: 10000, ch:0},{readOnly: true});
	}
	editors.push(editor);
    }
});

/**
 * allows one to edit the experiment givent by its primary key
 **/
function editExperiment(expId){
    $("input[name='expId2edit']").val(expId);
    var hiddenForm=$("#h");
    hiddenForm.submit();
}

/**
 * allows one to activate the experiment givent by its primary key
 **/
function activateExperiment(expId){
    $.post("/exp0/activateExperiment",
	   {
	       csrfmiddlewaretoken: csrf,
	       expId: expId,
	   }).done(
	       function(data){
		   window.location="/exp0/manage";
	       }
	   );
}

/**
 * allows one to duplicate the experiment givent by its primary key
 **/
function dupliExperiment(expId){
    $.post("/exp0/dupliExperiment",
	   {
	       csrfmiddlewaretoken: csrf,
	       expId: expId,
	   }).done(
	       function(data){
		   window.location="/exp0/manage";
	       }
	   );
}

/**
 * allows one to delete the experiment givent by its primary key
 **/
function deleteExperiment(expId){
    $.post("/exp0/deleteExperiment",
	   {
	       csrfmiddlewaretoken: csrf,
	       expId: expId,
	   }).done(
	       function(data){
		   window.location="/exp0/manage";
	       }
	   );
}

/**
 * callback function to edit an experiments's parameter
 * @param elt an element, which is supposed to bear the
 * attributes data-id, data-symbol, data-minval, data-maxval, data-option.
 **/
function editParam(elt){
    // inject current values into dialog's input fields
    var d= $("#paramDialog");
    var e=$(elt); // this element bears data in attributes data-*
    var names=['id', 'exp', 'symbol', 'minval', 'maxval', 'option'];
    for (var n=0; n < names.length; n++){
	d.find("input[name='"+names[n]+"']").val(e.attr("data-"+names[n]));
    }
    d.find("textarea[name='comment']").val(e.attr("data-comment"));
    var response="";
    var buttons=[];
    if (e.attr("data-id")){
	// the parameter has an id, so it can be deleted from the database
	buttons.push(buttonFactory("Delete"));
    }
    buttons.push(buttonFactory("Cancel"));
    buttons.push(buttonFactory("OK"));
    // create jquery-ui's dialog widget
    var dialog=d.dialog(
	{
	    title: "Edit parameter's bindings",
	    modal: true,
	    width: 380,
	    show: {duration : 400},
	    buttons: buttons,
	    beforeClose: checkDialogsValues(checks, success, deleteParam),
	}
    );

}

/**
 * a factory function to make buttons for jquery's dialog
 *
 * the only constraint is that the variable response must be valid
 * in the scope of the call of this factory; this variable will be
 * assigned the value of the 'text' parameter upon closing the dialog.
 * @param text the text of the button, and the value assigned to the
 *  variable response when the button is clicked.
 **/
function buttonFactory(text){
    return {
	text: text,
	click: function() {
	    /* the variable response must exist in the context
	     * where buttonFactory is called, to make the 'click'
	     * callback a valid closure */
	    response=text;
	    $( this ).dialog( "close" );
	}
    };
}


/**
 * Function factory which outputs an anonymous function with
 * a profile function(event, ui){} to be called back upon
 * some event of a jquery's dialog
 *
 * This function must be called inside a scope containing the variable
 * 'response' (which bears the text of the clicked button)
 * @param checks a function with profile void -> error string
 * which returns "ok" when the dialog can be close with a success
 * @param success a function with profile void -> void which will be
 * called if checks returned "ok"
 * @param deleteParam a function with profile void -> void which will
 * delete the parameter displaied in the dialog box.
 **/
function checkDialogsValues(checks, success, deleteParam){
    return function(event, ui){
	if (response != undefined && response=="OK"){
	    response=""; // reset response
	    var msg=checks();
	    if (msg=="ok"){
		success();
		return true;
	    } else {
		alert(msg);
		return false;
	    }
	} else if (response=="Delete"){
	    response=""; // reset response
	    deleteParam();
	} else {
	    // either Cancel or the close button were clicked
	    return true; // allow the dialog to be closed
	}
    }
}

/** a callback function which will be invoked when someone clicks
 * on the "OK" button of the dialog
 * @return a message, either "ok" if everythings is correct, or a
 * series of error lines.
 **/
var checks = function(){
    var d = $("#paramDialog");
    var errors=0;
    var msg="";
    if (d.find("input[name='symbol']").val()=="") {
	errors++;
	msg+="The symbol of the parameter cannot be empty, please fill in something.\n";
    }
    if ( isNaN(parseFloat(d.find("input[name='minval']").val()))) {
	errors++;
	msg+="The minimum value of the parameter must be a number.\n";
    }
    if ( isNaN(parseFloat(d.find("input[name='maxval']").val()))) {
	errors++;
	msg+="The maximum value of the parameter must be a number.\n";
    }
    // truncate the comment to 150 characters as it is the maximum
    // allowed in the model
    var t=d.find("textarea[name='comment']")
    var comment=t.val();
    t.val(comment.substr(0,150));
    if (errors==0) msg="ok"
    return msg;
}

/**
 * callback function to be called when the fields in the paramDialog
 * are parsed successfully.
 **/
function success(){
    var d= $("#paramDialog");
    $.post('/exp0/setParameter',{
	csrfmiddlewaretoken: csrf,
	id: d.find("input[name='id']").val(),
	exp: d.find("input[name='exp']").val(),
	symbol: d.find("input[name='symbol']").val(),
	minval: d.find("input[name='minval']").val(),
	maxval: d.find("input[name='maxval']").val(),
	option: d.find("input[name='option']").val(),
	comment: d.find("textarea[name='comment']").val(),
    }).done(
	function(){
	    // reload the page
	    window.location="/exp0/manage";
	}
    ). fail(
	function(data){
	    alert("Failure "+JSON.stringify(data));
	}
    );
}

/**
 * callback function to be called when the data of the dialog must
 * be erased from the database
 **/
function deleteParam(){
    var d= $("#paramDialog");
    $.post('/exp0/delParameter',{
	csrfmiddlewaretoken: csrf,
	id: d.find("input[name='id']").val(),
    }).done(
	function(){
	    // reload the page
	    window.location="/exp0/manage";
	}
    ). fail(
	function(data){
	    alert("Failure "+JSON.stringify(data));
	}
    );
}
