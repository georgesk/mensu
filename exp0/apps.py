from django.apps import AppConfig
from django.contrib.auth.models import Group, Permission

def populate_models(sender, **kwargs):
    # create groups
    print("Checking the groups: scientist, visitor")
    new_group, created = Group.objects.get_or_create(name='scientist')
    new_group, created = Group.objects.get_or_create(name='visitor')
    # assign permissions to groups
    # create users

class Exp0Config(AppConfig):
    name = 'exp0'

    def ready(self):
        post_migrate.connect(populate_models, sender=self)
        
