#!/usr/bin/python

### unfortunately, the toxml() does not work properly with Python3
### as from 2018 04 30, so I regress to Python2

from __future__ import print_function
import xml.dom.minidom, sys, os, base64, re

"""
This short program will find occurences of img src="some_file.png" in 
a html file, and replace them by data:URI strings in place.
"""

doc=xml.dom.minidom.parse(sys.argv[1])
images= doc.getElementsByTagName("img")

for i in images:
    src = i.getAttribute("src")
    if not src.startswith("data:image"):
        ext=os.path.splitext(src)[1].lower()
        if ext in ['.png','.jpg','.gif']:
            # an element img was detected, with an image of type png, jpg or gif
            # let us make a data:uri translation
            uri="data:image/" + ext[1:] + ";base64,"
            uri=uri.encode("utf-8")
            with open(src,"rb") as imgFile:
                uri+= base64.b64encode(imgFile.read())
            # then replace the image by its data:uri translation
            i.setAttribute("src", uri)
            # get all the document as a unicode string
            xml=doc.toxml()
            # insert the pandoc.css stylesheet at the right place
            pattern=re.compile(r"(<title>.*</title>)", re.I)
            xml=re.sub(pattern,"\\1\n"+open("pandoc.css").read().decode("utf-8"), xml)
            with open(sys.argv[1],"wb") as outfile:
                outfile.write(xml.encode("utf-8"))
