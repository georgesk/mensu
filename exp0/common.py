"""
Common data for the application "exp0"
"""

import os

SUMMARY_DIR = os.path.join(os.path.dirname(__file__), "summaries")
