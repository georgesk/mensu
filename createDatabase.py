#!/usr/bin/python3

import sys, argparse, gettext, os.path, subprocess
from mensu.settings import DATABASES

dbname    = DATABASES["default"]["NAME"]
dbuser    = DATABASES["default"]["USER"]
dbpassword= DATABASES["default"]["PASSWORD"]

gettext.bindtextdomain('mensu', os.path.join(os.path.abspath(__file__),"lang"))
gettext.textdomain('mensu')
_ = gettext.gettext

def deleteDb(args):
    """
    deletes a postgresql database.
    @param args a namespace instance, with variables name, user, password
    and a boolean delete == True.
    """
    assert (args.delete)
    try:
        p=subprocess.Popen(
            "su postgres -c psql", shell=True, stdin=subprocess.PIPE,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        sqlCommand="""\
DROP DATABASE {name};
DROP USER {user};
""".format(**vars(args))
        p.stdin.write(sqlCommand.encode("utf-8"))
        out,err=p.communicate()
        if out and out.strip():
            print(out[:-1].decode("utf-8"))
        if err and err.strip():
            print(err[:-1].decode("utf-8"))
        assert("FATAL" not in err[:-1].decode("utf-8"))
    except:
        print(_("""You are not allowed to delete the database "{name}".
You may want to run this command as user root.

Database "{name}" not deleted.""").format(**vars(args)))
    return

def updateSettings(args):
    """
    Updates the file mensu/settings.py
    @param args a namespace instance, with variables name, user, password
    and a boolean delete==False. When name, user, password are not the
    defaults defined in mensu/settings.py, this file is updated to fit
    the given values.
    """
    settingsFile=os.path.join(os.path.dirname(os.path.abspath(__file__)), "mensu", "settings.py")
    command="""sed -i -e "/^DATABASES/,+10 s/'NAME': .*/'NAME': '{name}',/" -e "/^DATABASES/,+10 s/'USER': .*/'USER': '{user}',/" -e "/^DATABASES/,+10 s/'PASSWORD': .*/'PASSWORD': '{password}',/" """.format(**vars(args)) + settingsFile
    p=subprocess.Popen(
        command, shell=True,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    out,err=p.communicate()
    if out and out.strip():
        print(out[:-1].decode("utf-8"))
    if err and err.strip():
        print(err[:-1].decode("utf-8"))
    return

def createDb(args):
    """
    creates a postgresql database.
    @param args a namespace instance, with variables name, user, password
    and a boolean delete==False. When name, user, password are not the
    defaults defined in mensu/settings.py, this file is updated to fit
    the given values.
    """
    assert (not args.delete)

    if args.name != DATABASES["default"]["NAME"] or \
       args.user != DATABASES["default"]["USER"] or \
       args.password != DATABASES["default"]["PASSWORD"] :
        updateSettings(args)

    try:
        p=subprocess.Popen(
            "su postgres -c psql", shell=True, stdin=subprocess.PIPE,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        sqlCommand="""\
CREATE USER {user} WITH PASSWORD '{password}';
CREATE DATABASE {name} OWNER {user};
""".format(**vars(args))
        p.stdin.write(sqlCommand.encode("utf-8"))
        out,err=p.communicate()
        if out and out.strip():
            print(out[:-1].decode("utf-8"))
        if err and err.strip():
            print(err[:-1].decode("utf-8"))
        assert("FATAL" not in err[:-1].decode("utf-8"))
    except:
        print(_("""You are not allowed to create the database "{name}".
You may want to run this command as user root.

Database "{name}" not created.""").format(**vars(args)))
    return

if __name__=="__main__":
    parser = argparse.ArgumentParser(
        description=_("Create the application's database"),
        prog="createDatabase",
    )
    parser.add_argument('user', nargs='?',
                        default=dbuser, help=_('User of the database'),
    )
    parser.add_argument('name', nargs='?',
                        default=dbname, help=_('Name of the database'),
    )
    parser.add_argument('--password', metavar=_("PASSWORD"),
                        default=dbpassword,
                        help=_('Please use a secured password (not "{p}" wich is the default)').format(p=dbpassword),
    )
    parser.add_argument('--delete', action="store_true",
                        help=_('delete the database "name" and the user "user"'),
    )
    args=parser.parse_args(sys.argv[1:])
    if args.delete:
        deleteDb(args)
    else:
        createDb(args)
    
