#!/usr/bin/python3

import xml.dom.minidom as minidom

import sys, re

doc=minidom.parse(open(sys.argv[1]))
groups=doc.documentElement.getElementsByTagName("g")
layerIds=[]
for g in groups:
    for i in range(g.attributes.length):
        a=g.attributes.item(i)
        if a.nodeName=="id" and re.match("^layer",a.nodeValue):
            layerIds.append(a.nodeValue)
print(",".join(layerIds))

