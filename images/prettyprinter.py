#!/usr/bin/python3

import xml.dom.minidom as minidom

import sys

doc=minidom.parse(open(sys.argv[1]))
print(doc.documentElement.toprettyxml(indent="  "))
