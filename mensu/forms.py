from django.utils.translation import gettext as _
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

import os
from PIL import Image

from .settings import BASE_DIR

_max_size=(300,400)

class ProfileForm(forms.Form):

    first_name=forms.CharField(max_length=30, label=_('Your first name'))
    last_name=forms.CharField(max_length=30, label=_('Your last name'))
    email=forms.EmailField(label=_('Your e-mail address'))
    image=forms.FileField(
        label=_('A small image as avatar (max-size: {w} x {h} px)').format(
            w=_max_size[0], h=_max_size[1]),
        required=False
    )

    def clean_image(self):
        image=self.cleaned_data['image']
        if image==None:
            return image # this field is not required
        im=None
        try:
            im=Image.open(image)
        except:
            raise ValidationError(_("'{}' was not recognized as an image file. Check the file's type.").format(image))
        if im.size[0]>_max_size[0] or im.size[1] > _max_size[1]:
            raise ValidationError(_("The size of the image '{i}' is {iw} x {ih} px, which is bigger than the maximum allowed size: {w} x {h} px.").format(
                i=image, iw=im.size[0], ih=im.size[1], w=_max_size[0], h=_max_size[1]))
        return image
    
