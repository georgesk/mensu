"""
Version number, as decided by upstream
"""

upper="0"
lower="1"
codename="alpha"


licence = "GNU Public License, version 3+ (GPL-3+)"
copyright = "© 2018 Georges Khaznadar <georgesk@debian.org>"
