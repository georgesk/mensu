/**
 * Javascript code available for the Mensu service
 * it supposes that $ is jQuery and that jquery-ui is working
 **/

var thegroups=""; // groups of the current user

$(function(){
    // actions to be done when the window is loaded
    // hide everything hidable, and bind toggle methods to
    // elements with btn class
    $(".hidable").hide(0);
    $(".toggleBtn").click(
	function(){
	    var p=$(this);
	    while (! p.hasClass("hidableContainer")){p=p.parent();}
	    var hidable=p.find(".hidable");
	    hidable.toggle(600);
	}
    );
    thegroups = $("#thegroups").val();
    var ifgroup=$(".ifgroup"); // elements whose vivibility depends on groups
    for (var i=0; i < ifgroup.length; i++){
	var el=$(ifgroup[i]);
	var group=el.attr("data-group");
	if (thegroups.indexOf(group) < 0){
	    el.addClass("disabled");
	}
    }
});
