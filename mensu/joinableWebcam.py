#!/usr/bin/python3

import cv2, time, sys, datetime, pytz, re, os
#import numpy as np
from threading       import Thread
from subprocess import Popen, call, PIPE

TIME_ZONE="Europe/Paris"


class WebcamInThread(Thread):
    
    def __init__(self, capture):
        """
        registers an already open video camera
        """
        Thread.__init__(self)
        self.stream=capture
        self.ok=True
        self.errorMsg=""
        # read one image from the stream
        ret,self.frame=self.stream.read()
        if not ret:
            self.ok=False
            self.errorMsg="No image coming from the webcam. However, at least one image was obtained previously"
            raise Exception(self.errorMsg)
        self.end=False
        self.daemon=True
        return
    
    def run(self):
        """
        grab images in a loop intil self.end
        """
        while not self.end:
            ret, frame=self.stream.read()
            if ret:
                self.frame=frame
            else:
                self.ok=False
                self.errorStr="The webcamm missed an image. The frame may be frozen."
        return
    
    def lastFrame(self):
        return self.frame
    
    def stop(self):
        self.end=True # will stop the loop


class JoinableWebcam:
    """
    a class to manage a webcam in a way which allows to ask it
    a video or an image. The real webcam is hidden by a layer of
    abstraction: original images are grabbed in a separate thread.
    
    A JoinableWebcam instance provides a few methods:
      * video to make a video file
      * image to make an image file
      * stop to let it realease the webcam.
    """
    
    def __init__(self, device=None, framerate=25):
        """
        The constructor
        @param device the webcam to use, defaults to None; if None, the first
          available device (/dev/video*) will be taken
        @param framerate the number or requires frames per second, defaults
          to 25. If the webcam is slower, some frames will be duplicated
          silently.
        """
        if device==None:
            r, _ =Popen("ls /dev/video*", shell=True, stdout=PIPE).communicate()
            r=r.decode("utf-8").split("\n")
            if len(r)>0:
                device=r[0]
        self.cap=cv2.VideoCapture(device)
        ret,img=self.cap.read()
        height , width , layers =  img.shape
        self.size=(width, height)
        self.framerate=framerate
        self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.wit=WebcamInThread(self.cap)
        self.wit.start()
        return

    def video(self, outfile="output1.avi", fourcc=None, duration=3,
              timestamp=True):
        """
        Writes a video file.

        N.B.: due to openCV's limitations, the video file is first
        recorded as MJPG-encoded AVI file, then recoded to V8-encoded
        WEBM file, for a better compression and compatibility with
        HTML readers.

        @param outfile the name of the output file, defaults to "output1.avi"
        @param fourcc the video encoder; defaults to MJPG
        @parameter duration the duration of the video; defaults to 3 seconds
        @param timestamp adds a timestamp if True (by default)
        """
        if fourcc==None:
            fourcc=cv2.VideoWriter_fourcc(*'MJPG')
        writer=cv2.VideoWriter(outfile, fourcc, self.framerate, self.size)
        startTime=time.time()
        t=startTime
        i=0
        while time.time()-startTime < duration:
            # wait a little to comply to the wanted framerate
            while time.time()-startTime < i/self.framerate :
                pass
            i+=1
            # read the last frame from the webcam in a thread
            f=self.wit.lastFrame()
            if not self.wit.ok:
                if "No such device" in self.wit.errorMsg:
                    print("Webcam device lost, exiting")
                    self.wit.end=True
                    self.wit.join()
                    sys.exit(1)
                else:
                    raise Exception(self.wit.errorMsg)
            if timestamp:
                self.timestamp(f)
            writer.write(f)
        writer.release()
        cmd="ffmpeg -i {avi} -vcodec libvpx -cpu-used 4 -threads 8 {webm}".format(avi=outfile, webm=re.sub(r"\.avi$", ".webm", outfile))
        call(cmd,shell=True)
        os.unlink(outfile)
        return

    def timestamp(self, f):
        """
        adds a timestamp on a frame
        @param f a frame
        """
        dt=datetime.datetime.now(tz=pytz.timezone(TIME_ZONE))
        t=re.sub(r"(\.\d\d)\d\d\d\d",r"\1 ",dt.isoformat(' '))
        cv2.putText(f,t,(10,20), self.font,
                    0.8,(0,0,0),4,cv2.LINE_AA)
        cv2.putText(f,t,(10,20), self.font,
                    0.8,(255,255,255),2,cv2.LINE_AA)
        
    
    def getImage(self, timestamp=True):
        """
        gets an image as a memory object (openCV frame)
        @param timestamp adds a timestamp if True (by default)
        """
        f=self.wit.lastFrame()
        if not self.wit.ok:
            if "No such device" in self.wit.errorMsg:
                print("Webcam device lost, exiting")
                self.wit.end=True
                self.wit.join()
                sys.exit(1)
            else:
                raise Exception(self.wit.errorMsg)
        if timestamp :
            self.timestamp(f)
        return f

    def image(self, outfile="output1.jpg", timestamp=True):
        """
        writes an image file
        @param outfile the name of the output file, defaults to "output1.jpg"
        @param timestamp adds a timestamp if True (by default)
        """
        cv2.imwrite(outfile,self.getImage(timestamp))
        return

    def stop(self):
        self.wit.stop()
        self.wit.join()
        self.cap.release()

def test():
    class Chrono:
        def __init__(self):
            self.start=time.time()
        def t(self):
            return "%5.2f" % (time.time()-self.start)
        def stamp(self, s):
            print( self.t()+": "+s)
            return

    print("Starting the main program")
    c=Chrono()

    jw=JoinableWebcam(0,25) # /dev/video0, 25 fps
    c.stamp("the JoinableWebcam is initialized")
    c.stamp("ask the image 'output1.jpg'")
    jw.image("output1.jpg")
    c.stamp("ask the default video, default duration")
    jw.video()
    c.stamp ("ask the video \"output2.avi\", duration = 3 seconds")
    jw.video(outfile="output2.avi",duration=3)
    c.stamp ("stop the webcam")
    jw.stop()
    c.stamp("Ending the main program")
    return

import select
def standalone():
    """
    Runs as a standalone application. Scrutinizes a named pipe, given
    as the first argument. When no argument is given, scrutinizes
    the standard input.
    Recognizes the following orders:
      * <somefile>.avi : create a video file with the default duration (3s)
      * <somefile>.avi <number> : create a video file with the given duration
      * <somefile>.jpg : create an image
    """
    ## create the joinable webcam instance
    jw=JoinableWebcam(device=None, framerate=25) # first /dev/video*, 25 fps
    ## bind to the in and out pipes (create them if necessary)
    if len(sys.argv)>1:
        if not os.path.exists(sys.argv[1]):
            os.mkfifo(sys.argv[1])
        inpipe=open(sys.argv[1])
    else:
        inpipe=sys.stdin
    if len(sys.argv)>2:
        if not os.path.exists(sys.argv[2]):
            os.mkfifo(sys.argv[2])
        outpipe=open(sys.argv[2],"w")
    else:
        outpipe=sys.stdout
    ## main loop : non-blocking select.select call, with timeout 0.2 s.
    while True:
        inlines, _, _ = select.select([inpipe], [], [], 0.2)
        if inlines:
            iterline = list(iter(inlines[0].readline, ""))
            ###### remove duplicate calls #######
            deduplicated=[]
            for l in iterline:
                if not l in deduplicated:
                    deduplicated.append(l)
            for l in deduplicated:
                l=l.strip()
                if l=="":
                    print("Closing", file=outpipe)
                    sys.exit(0)
                print("JW got:", l, file=outpipe)
                ######### trying to match a video #################
                m=re.match(r"(\S+\.avi)(\s+([.0-9]+))?", l)
                if m:
                    filename=m.group(1)
                    duration=3
                    if len(m.groups())>2:
                        try:
                            duration=float(m.group(3))
                            assert (duration > 1)
                        except:
                            pass
                    jw.video(filename, duration=duration)
                    print("JW did: {v} {d}".format(v=filename, d=duration))
                ######### trying to match an image #################
                m=re.match(r"(\S+\.jpg)", l)
                if m:
                    filename=m.group(1)
                    jw.image(filename)
                    print("JW did: {i}".format(i=filename))
                    

if __name__=="__main__":
    #test()
    standalone()

