from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.sites.shortcuts import get_current_site
from allauth.account.models import EmailAddress
from django.contrib.auth.models import User, Group
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.core.mail import EmailMessage
from django.contrib.auth import authenticate, login
from allauth.account.models import EmailAddress

import os

from .tokens import account_activation_token
from .forms import ProfileForm
from exp0.models import ImageUser
from mensu.settings import MEDIA_URL

import mensu.version


def home(request):
    return render(
        request, 'home.html',
        {
        }
    )

def p404(request):
    return render(
        request, '404.html',
        context={
            "path": request.path
        },
        status=404,
    )

def about(request):
    return render( request, 'about.html', {"version": mensu.version})

def license(request):
    return render(request, 'license.html', {
        "license" : open("/usr/share/common-licenses/GPL-3").read(),
        "version": mensu.version,
    })

def manual(request):
     return render( request, 'manual.html', {})
   
def makeProfile(request):
    """
    Specific actions when some user has signed up, or ...
    """
    if request.method=="POST":
        form=ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            ## save the image
            print ("GRRRR request.FILES=", request.FILES)
            image=request.FILES.get('image',"")
            imageUsers=ImageUser.objects.filter(user=request.user)
            if imageUsers:
                # an image exists already for this user:
                # remove the older image and save the newest one
                imageUser=imageUsers[0]
                if image:
                    if os.path.isfile(imageUser.image.path):
                        os.remove(imageUser.image.path)
                    imageUser.image=image
                imageUser.save()
            else:
                # record a new image for this user
                if image:
                    imageUser=ImageUser(user=request.user, image=image)
                else:
                    imageUser=ImageUser(user=request.user)
                imageUser.save()
            ## save the other data to user's table
            request.user.first_name=form.cleaned_data["first_name"]
            request.user.last_name=form.cleaned_data["last_name"]
            request.user.email=form.cleaned_data["email"]
            request.user.save()
            ## save email to account/email table
            email=EmailAddress.objects.get(user=request.user)
            if email.email!=form.cleaned_data["email"]:
                email.email=form.cleaned_data["email"]
                email.verified=False
                email.primary=False
                email.save()
                return sendConfirmation(request, form)
            else:
                # the email has not changed in the account/email
                # send a confirmation e-mail if account/email is not verified
                if not email.verified:
                    return sendConfirmation(request, form)
        else:
            return render( request, 'profile.html', {
                "form": form,
            })         
    else:
        uploaded_file_url = ""
    images=ImageUser.objects.filter(user=request.user)
    account_emails=EmailAddress.objects.filter(email=request.user.email)
    if not account_emails:
        account_email=EmailAddress(email=request.user.email, user=request.user)
        account_email.save()
    else:
        account_email=account_emails[0]
    email=account_email
    if images:
        image=images[0].image
    else:
        image=None
    form=ProfileForm({
        "first_name": request.user.first_name,
        "last_name": request.user.last_name,
        "email": request.user.email,
    })
    ### Check whether we can subscribe this user to the group "visitor":
    if request.user.first_name and request.user.last_name and image and email.verified:
        ## whe have a profile sufficient to subscribe the user as visitor
        visitor = Group.objects.get(name='visitor') 
        visitor.user_set.add(request.user)
    return render( request, 'profile.html', {
        "form": form,
        "email": email,
        "image": str(image),
        "media_url": MEDIA_URL,
    })

def sendConfirmation(request, form):
    """
    sends a confirmation email
    @param request the request context of a user
    @param form other data coming from the form filled in accounts/profile/
    """
    user=request.user
    current_site = get_current_site(request)
    mail_subject = 'Activate your MENSU account.'
    uid=urlsafe_base64_encode(force_bytes(user.pk))
    token=account_activation_token.make_token(user)
    message = render_to_string('confirmation_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': uid,
        'token': token,
    })
    to_email = form.cleaned_data.get('email')
    email = EmailMessage(
        mail_subject, message, to=[to_email]
    )
    email.send()
    return HttpResponse('Please confirm your email address to complete the registration')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        ## validate the user's Email address
        emailAddress=EmailAddress.objects.get(user=user)
        emailAddress.verified=True
        emailAddress.primary=True
        emailAddress.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        # return redirect('home')
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')
    
